# Summary

* [NCS in Kubernetes](README.md)
* [Why Kubernetes?](chapter1.md)
* [NCS Runtime Architecture](section.md)
* [Deployment and management](deployment-and-management.md)
* [Site and POD components](site-and-pod-components.md)
* [Installing Nuance Cloud Services](installing-nuance-cloud-services.md)
* [NCS distributions to be deployed](ncs-distributions-to-be-deployed.md)

