# Site and POD components

Rev2 Sent for review: JIRA / Evans / Tuesday, October 17, 2017 \(also on voice: [https://nuance.jiveon.com/docs/DOC-174129\](https://nuance.jiveon.com/docs/DOC-174129\)\)

TheNuance Cloud Servicessite consists of one or more PODs. The components deployed at site level and POD level are listed below. 

### Components at the site level

* **Infrastructure required to deploy and manage the NCS components**  
  These components include SSPNI Jump Box, Deploy Tool, Docker Registry Proxy, GITLab, and Artifactory Pro.

* **Data service infrastructure**  
  :

  * Log Management 
    &
     Intelligence Solution
    \(
    LMIS
    \) and Hadoop.
    LMIS
    and the Hadoop cluster are dedicated to data warehousing, advanced troubleshooting via call logs, and speaker dependent acoustic model adaptation.
  * Cassandra database. Stores user data such as contact lists.
  * ELK monitoring stack
    \(per cloud?\)

### Components at POD level

* Kubernetes cluster infrastructure: Kubernetes Master, kubelet kube-proxy and Kube Node
* Kubernetes-managed NCS  
  product  
  s included in the distribution:

* the Common Services  
  product  
  and the Core  
  product  
  and:

* * optionally, the TTS
    product
  * optionally, the NLPS
    product
  * optionally, the DCS
    product

or

* the Common Services  
  product  
  and the DCS  
  product

* Zookeeper and the NMAS database, which are not managed in Kubernetes.

For short descriptions of infrastructure components at the site and POD levels, see[Deployment and management architecture](K8_intro_arch_infrastructure.html)

For NCS components per product, see[Nuance Cloud Services runtime architecture](k8_intro_arch_rt_diagram_components.html)

