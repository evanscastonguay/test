# Why Kubernetes?

In this release, NCS services are deployed on the Azure public cloud using Docker containers in a Kubernetes cluster. This implementation builds on the strengths of the previous NCS architecture, namely, high availability, low latency, manageability and scalability:

* **Unified process:**
  Kubernetes clusters can be deployed on public clouds or Nuance data centers with minimal differences in configuration and deployment process.

* **Simpler runtime architecture:**
  Previously, the number of hosts that could be added to a single Management Station was limited. Traffic was routed to multiple runtime clusters, each with its own Management Station. In the NCS Kubernetes architecture, runtime clusters have been eliminated;: all traffic that accesses the same services can be accommodated in the same POD. Within the POD, namespaces can be used to isolate traffic where required.
* **Better resource utilization:**
  Instances of NCS components can be added, removed, and redistributed with ease:
  * Containers provide the system requirements for running components with different requirements on the same host.
  * Identifying hosts for service deployments is handled by Kubernetes.
* **Fault tolerance:**
  Kubernetes maintains the specified number of instances \(replicas\) of each component, automatically deploying new instances when instances fail.
* **Rolling upgrades:**
  Kubernetes has been leveraged to automatically orchestrate and perform the migration from software release A to release B on a node-by-node basis.
* **Advantage of public cloud tenancy:**
  Hardware requirements for traffic increases and decreases are handled by renting additional virtual machines rather than by acquiring additional hosts.

## Useful links about Kubernetes

Rev1 Needs review: Jira Number

