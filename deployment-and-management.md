# Deployment and management architecture

Rev2 Sent for review: JIRA / Evans/ Tuesday, October 17, 2017 \(also on voice: https://nuance.jiveon.com/docs/DOC-174129\)

The following components are used to deploy NCS. They remain present in the fully deployed system to manage deployment of new datapacks, updates, scaling of services, etc.

![](/assets/ncs7.0100_arch_deploy.png)

Component Descriptions:

|  | **Montreal deployment components** |
| :--- | :--- |
| GIT repo | Five repos:**mobility-deployment-products**: the source of the defaultproductconfiguration and component manifests.**mobility-deployments-environments/&lt;location\_distribution&gt;**: the source of the configuration of the distribution \(the set of products\) to be deployed to a specific environment. \(The Montreal lab repo contains configuration files for model environments and for specific production and testing environments.\)**mobility-deployment-archives/&lt;location\_distribution&gt;**: contains the record of the deployment in the form of product manifests containing the property values specified for deployed environments in Montreal. Read only.**nlps-profiles-dev/&lt;customer release&gt;**: Source of qualified profiles for customer deployments.**dcs**: present if DCS is included in the environment. Source of the default configuration for DCS. |
| Bamboo/bamboo agent | Proprietary central user interface for managing the deployment of products and distributions to environments, including the CI/CD pipeline deployments. \(The CI/CD pipeline automatically tests and promotes specific builds of components, products and distributions.\) |
| Artifactory Pro Docker Registry service | Stores the docker images of services to be deployed. These images are uploaded to the registry automatically after successful CI pipeline automated regression testing. |
| Artifactory Pro packs service | Source of datapacks used by NCS services. |
|  | Artifactory and Docker Registry in Montreal are the repositories for all deployment artifacts. |
|  | **Site deployment components** |
| Git repos | Four repos:**mobility-deployment-products**: the source of the defaultproductconfiguration and component manifests. Mirror of the Montreal Lab repo.**mobility-deployment-environments/&lt;location-distribution&gt;**: the source of the specific configuration of distribution \(the set of products to be deployed\) for the remote environment. Mirror of the Montreal Lab repo.**mobility-deployment-archives&lt;location-distribution&gt;**: contains the record of the deployment in the form of manifests containing the property values specified for this remote environment. Content is generated on the site.**dcs**: present if DCS is included in the environment,. Source of the default configuration for DCS. Mirror of the Montreal Lab repo.**nlps-profiles-dev/customer release**:Contents are updated during the Promotion stage of deployment runs. |
| JumpBox | An SSH server used to access other servers in another security zone, in this case, the remote network on the Azure public cloud. |
| Deploy Tool | NCS tool for Kubernetes installs: Runs the deploy script which extracts parameters from configuration files, generates component manifests, and deploys the product or distribution ot the environment..Includes: crypto tool for encrytping and decrytping confidential information. |
| Artifactory Pro \(Docker Registry\) | Stores the docker images of services to be deployed. Contents are promoted to the site as the result of successful CI/CD pipeline runs. |
| Artifactory Pro \(packs\) | Source of versioned packs and other artifacts used by NCS services. This is a mirror of the Artifactory in Montreal. Contents are promoted to the site as the result of successful CI/CD pipeline runs. |
|  | POD deployment components |
| Salt Master host | \(not shown\) Host where Salt Master is located. Salt Master is used to install Kube-master and Kube-node hosts and acts a a central control bus for its clients \(minions\). Salt minions are located on each of the Kube hosts and are used to install and configure applications on these hosts. |
| Kube Master host | Manages the Kubernets cluster: makes global decisions about the cluster such as scheduling, and detecting and responding to cluster events, or example, starting up a new pod when a replication controller's 'replicas' field is unsatisfied\). |
| etcd | The component includes:**etcd:**key value store which Kubernetes uses for persistent storage of all of its REST API objects**flanneId:**used as networking layer for kubernetes**kube-apiserver:**exposes the Kubernetes API**kube-controller-manager:**binary that runs controllers \(the background threads that handle routine tasks in the cluster\)**kube-skydns:**provides local DNS to the Kubernets cluster.**kube-scheduler:**assigns nodes for new pods to run on.**kube-ui:**View of Kubernetes cluster available via web browsers.**cockpit:**Administration tools available via web browsers. |
| Kube Node | A host where Kubernetes pods are running.**kubelet:**primary node agent. It:Watches for pods that have been assigned to its node \(either by apiserver or via local configuration file\) and runs the pod's containers via dockerMounts the pod's required volumes, downloads the pod's secretsPeriodically executes requested container liveness probes.Reports the status of the pod and node back to the rest of the system**kube-proxy:**enables the Kubernetes service abstraction by maintaining network rules on the host and performing connection forwarding.**docker:**runs containers. |



