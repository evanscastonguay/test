# Nuance Cloud Services runtime architecture

\(The architecture is based on a diagram from July. It is not up to date conceptually.\)

This figure shows the runtime recognition and content architecture for a single site in the context of the sites within a region that communicate with the region's research grid.

There are two entry points, Dragon Connected Services\(DCS\) commands and NCS commands. These commands originate in client applications residing on handsets, automobile head units, etc.

Components managed outside of Kubernetes are shown in blue■.

The table that follows provides a brief description of the runtime components and their functions. \(Also see[Detailed description of product components](../intro_components.html#nvcp_components_1040291522_775117).\)

\(For information on the offline adaptation components, please see[Offline adaptation systems](../intro_components_offlineAdapt.html).\)

![](/assets/ncs7.0100_arch_POD.png)NCS components managed by Kubernetes

### NTG6components, per product \(sub-namespace\):

#### Core product

HA-proxy: an Azure specific ingress controller

HTTP server

Gateway, Media gateway and Websockets Proxy

Log Concentrator \(LC\)

Mobile ZooKeeperMZK\)

Nuance Mobile Speech Platform Gateway\(NMAS\)

Profile Content Server\(PCS\)

PCS proxy

Customer Conifguration Storage Server\(CCSS\)

Natural Language Generation Service \(NLGS\)

LLMS

HTTP Server \(not shown\)

#### NSSMTPS \(sub-namespace\)

NSS and MTPS are part of the Core product. They can be installed in a separate sub-nanamespace or in core.

Mobile TextProc Server\(MTPS\)

Nuance Speech Server\(NSS\)

#### NLPS product

Log Concentrator \(lc\)

VCSLocalSearch

VCSLocation

Profiles Manager

Transcription Cloud Services\(TraCS\)

NLPS Automotive

#### TTSproduct

Vocalizer for Cloud Services\(VoCS\)

### Common Services \(comsvdistro\) components

Common Services is a distribution that consists of three sub-namespaces containing five products:

**FileBeat**and**DataVolumeCleaner**, which are installed on every node in the Kubernetes cluster. \(Not shown in the runtime diagram above\)

MPDSS\(an NLPS component\), andNCS Storage Cleaner\(NCS SC\) \(deletes aged user information in Cassandra\) are each installed on one node in the Kubernetes cluster.

**HTTP Server**, installed as pool\(s\) in the cr sub-namespace. \(Not shown in the diagram above\)

## NCS components not managed by Kubernetes

NMAS database

Apache Zookeeper

Log Management & Intelligence Solution\(LMIS\)

Hadoop cluster

Cassandra database

ELK, Visibility \(not shown\)

  




