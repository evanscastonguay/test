# NCS distributions to be deployed

Rev4 Reviewed by Evans./ Monday, October 19, 2017

This section covers the 7.0.100 installation of NCS components that are managed in Kubernetes on the Azure public cloud. These components are deployed in two distributions:



* **comsvdistro:**
  consists of the following components: FileBeat, DataVolumeCleaner,
  NCS Storage Cleaner
  , and
  MPDSS
  \(an NLPS component\).
* **NTG6**
  **:**
  consists of the Core \(including NSSMTPS\), NLPS, TTS and DCS
  product
  s. For the list of components in these products, see
  [Nuance Cloud Services runtime architecture](k8_intro_arch_rt_diagram_components.html)
  .



Note:The comsvdistro andNTG6sets of NCS components to be deployed are referred to as the "distributions" . The configuration requirements based on the location a distribution will be deployed to is referred to as the "environment."

Rev1 Needs review: SSPNI-1671 \(MPDSS in comsvdistro\)

**Prerequisite installations not covered in thisdocument.**The following components are required as prerequisites to the deployment of the NCS distributions. The procedures in this document assume that these components have already been deployed in Kubernetes:

* Deployment and management infrastructure on the remote site:
  * SSPNI Jumpbox
  * Artifactory Pro
  * Docker Registry
  * GitLab
  * Bamboo Agent and the Deploy Tool

* Kubernetes Cluster and infrastructure:
  * Salt Master
  * Kube master
  * Kube nodes

* NCS components \(includes some runtime, call log collection and merging, and metrics collection and monitoring components\)
  * NMAS database
  * Zookeeper
  * Cassandra database
  * Hadoop
  * LMIS
  * Visibility

Also see[Checklist: Prerequisite information -incomplete](k8_install_checklist_highLevel.html)

